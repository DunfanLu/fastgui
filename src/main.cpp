#include "Utils.h"
#include "GUI.h"
#include <vector>
#include <pybind11/pybind11.h>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;

struct PyGUI : public GUI{
    PyGUI(std::string name, py::tuple res): GUI(name,res[0].cast<int>(),res[1].cast<int>()){

    }

    // this is so that the GUI class does not need to use any pybind related stuff
    py::tuple py_get_cursor_pos(){
        auto pos = get_cursor_pos();
        float x = std::get<0>(pos);
        float y = std::get<1>(pos);
        return py::make_tuple(x,y);
    }
};

PYBIND11_MODULE(_impl, m) {
    py::class_<PyGUI>(m, "GUI")
        .def(py::init<std::string, py::tuple>())
        .def("set_image", &PyGUI::set_image)
        .def("triangles",&PyGUI::triangles)
        .def("show", &PyGUI::show)
        .def("is_pressed", &PyGUI::is_pressed)
        .def("get_cursor_pos", &PyGUI::py_get_cursor_pos)
        .def("is_running", &PyGUI::is_running)
        .def("get_event", &PyGUI::get_event)
        .def_property("event", &PyGUI::getCurrentEvent,&PyGUI::setCurrentEvent)
        ;
    
    py::class_<Event>(m,"Event")
        .def_property("key", &Event::getKey,&Event::setKey);

    m.attr("DTYPE_F32") = py::int_(DTYPE_F32);
    m.attr("DTYPE_U8") = py::int_(DTYPE_U8);
    m.attr("DTYPE_I32") = py::int_(DTYPE_I32);
    m.attr("EVENT_PRESS") = py::int_(EVENT_PRESS);
    m.attr("EVENT_NONE") = py::int_(EVENT_NONE);
}
