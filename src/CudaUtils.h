#pragma once

#include <device_launch_parameters.h>
#include <cuda_gl_interop.h>


inline static void HandleError(cudaError_t err, const char* file, int line) {
	if (err != cudaSuccess) {
		printf("CUDA error: %s in %s at line %d\n", cudaGetErrorString(err), file, line);
		exit(EXIT_FAILURE);
	}
}
inline void __getLastCudaError(const char *errorMessage, const char *file, const int line)
{
    cudaDeviceSynchronize();
    cudaError_t err = cudaGetLastError();

    if (cudaSuccess != err)
    {
        fprintf(stderr, "%s(%i) : getLastCudaError() CUDA error : %s : (%d) %s.\n",
                file, line, errorMessage, (int)err, cudaGetErrorString(err));
        cudaDeviceReset();
        exit(EXIT_FAILURE);
    }
}
// This will output the proper error string when calling cudaGetLastError
#define CHECK_CUDA_ERROR(msg) __getLastCudaError (msg, __FILE__, __LINE__)

#define HANDLE_ERROR( err ) \
    cudaDeviceSynchronize(); \
    HandleError( err, __FILE__, __LINE__ )


#define HANDLE_NULL( a ) {\
	if (a == NULL) { \
		printf( "Host memory failed in %s at line %d\n",  __FILE__, __LINE__ ); \
		exit( EXIT_FAILURE ); \
	}\
}


#define MAX_THREADS_PER_BLOCK 1024
inline void setNumBlocksThreads(int N,int& numBlocks, int& numThreads) {
    numThreads = min(N, MAX_THREADS_PER_BLOCK);
    numBlocks = divUp(N, numThreads);
}