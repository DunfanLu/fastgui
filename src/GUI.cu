#include "GUI.h"
#include "CudaUtils.h"
#include "Quad.h"
#include "Triangle.h"

template<typename T>
__device__
inline unsigned char getValue(T x);

template<>
__device__
inline unsigned char getValue<unsigned char>(unsigned char x){
    return x;
}

template<>
__device__
inline unsigned char getValue<float>(float x){
    x = max(0.f,min(1.f,x));
    return (unsigned char)(x * 255);
}


template<typename T>
__global__
void copyToTextureBuffer(T* src, cudaSurfaceObject_t surface, int W, int H, int actualW, int actualH, int channels){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i >= W*H) return;

    int y = i / W;
    int x = i % W;

    T* src_base_addr = src + (x*actualH + y) * channels;
    uchar4 data = make_uchar4(0,0,0,0);
    
    data.x = getValue<T>(src_base_addr[0]);
    data.y = getValue<T>(src_base_addr[1]);
    data.z  = getValue<T>(src_base_addr[2]);
    data.w = 255;

    surf2Dwrite(data, surface, x* sizeof(uchar4), y);
    //surf2Dwrite(make_uchar4(x,min(255,y),0,255), surface, x* sizeof(uchar4), y);
}


template<typename T>
__global__
void copyToVertexBuffer(T* srcA, T* srcB, T* srcC, int numTriangles, float2 *pos){
    // copy filed : `src` to  pos list
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int vbIdx = 3 * i;
    int posIdx = 2 * i;
    pos[vbIdx + 0].x = srcA[posIdx];
    pos[vbIdx + 0].y = srcA[posIdx+1];
    pos[vbIdx + 1].x = srcB[posIdx];
    pos[vbIdx + 1].y = srcB[posIdx+1];
    pos[vbIdx + 2].x = srcC[posIdx];
    pos[vbIdx + 2].y = srcC[posIdx+1];
}

GUI :: GUI(std::string name_, int W_, int H_):name(name_),W(W_),H(H_){
    window = createWindowOpenGL(name,W,H);
    setCallbacks();
    initTexture();
    quad = new Quad();
    tris = new Triangle();
    initCudaTexture();
    initVertexBuffer();
    // printf("created cuda tex\n");
    lastRecordTime = glfwGetTime();
}


void GUI::setCallbacks(){
    glfwSetKeyCallback(window, InputHandler::keyCallback);
    glfwSetCursorPosCallback(window, InputHandler::mousePosCallback);
    glfwSetMouseButtonCallback(window, InputHandler::mouseButtonCallback);

    InputHandler::Handler::instance().userKeyCallbacks.push_back([&](int key, int action){
        if(action == GLFW_PRESS){
            events.push_back({EVENT_PRESS,buttonIDToName(key)});
        }
    });
    InputHandler::Handler::instance().userMouseButtonCallbacks.push_back([&](int key, int action){
        if(action == GLFW_PRESS){
            events.push_back({EVENT_PRESS,buttonIDToName(key)});
        }
    });
}

void GUI::initTexture(){
    
    glEnable(GL_TEXTURE_2D);
    glGenTextures(1,&textureID);
    glBindTexture( GL_TEXTURE_2D, textureID);
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, W, H, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
}

void GUI::drawBuffer(){
    //.HANDLE_ERROR(cudaGLUnmapBufferObject(bufferID));

    quad->draw(textureID);
}

void GUI::set_image(uint64_t image,int channels,int dtype){
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glClearColor(0,0,0,1);

    //unsigned char* buffer = getBufferAddress();

    int actualW = nextPowerOf2(W);
    int actualH = nextPowerOf2(H);

    // actualH = max(actualH,actualW);
    // actualW = actualH;

    int pixels = W*H;
    int numBlocks,numThreads;
    setNumBlocksThreads(pixels,numBlocks,numThreads);

    if(dtype == DTYPE_U8){
        copyToTextureBuffer<<<numBlocks,numThreads>>>((unsigned char*)image,(cudaSurfaceObject_t)surface,W,H,actualW,actualH,channels);
    }
    else if (dtype == DTYPE_F32){
        copyToTextureBuffer<<<numBlocks,numThreads>>>((float*)image,(cudaSurfaceObject_t)surface,W,H,actualW,actualH,channels);
    }

    cudaDeviceSynchronize();
    CHECK_CUDA_ERROR("copy to texture\n");
    
    drawBuffer();
}

void GUI::triangles(uint64_t vert1Addr, uint64_t vert2Addr, uint64_t vert3Addr,int dtype, int numTriangles, uint32_t color)
{
    triColor = color;

    // Check if the vertex buffer is large enough to store n triangles,
    // if not, we need to recreate a new vertex buffer
    if(numTriangles > numTris)
        updateVertexBuffer();

    numTris = numTriangles;
    if(dtype == DTYPE_F32){
        // map OpenGL buffer object for writing from CUDA
        float2 *dptr;
        HANDLE_ERROR(cudaGraphicsMapResources(1, &cuda_vbo_resource, 0));
        size_t num_bytes;
        HANDLE_ERROR(cudaGraphicsResourceGetMappedPointer((void **)&dptr, &num_bytes,
                                                            cuda_vbo_resource));
        // printf("CUDA mapped VBO: May access %ld bytes, number of triangles: %d\n", num_bytes, numTriangles);

        int numThreads = 256;
        int numBlocks = (numTriangles + numThreads)/numThreads;
        copyToVertexBuffer<<< numBlocks, numThreads>>> ((float*)vert1Addr,(float*)vert2Addr,(float*)vert3Addr, numTriangles,dptr);
        cudaDeviceSynchronize();
        // unmap buffer object
        HANDLE_ERROR(cudaGraphicsUnmapResources(1, &cuda_vbo_resource, 0));
    }
    else if (dtype == DTYPE_I32)
    {
        // TODO: Don't support this type right now
    }
    // Draw triangles
    tris->draw(triColor, numTris);
}


void GUI::show(){
    ++framesSinceLastRecord;

    double currentTime = glfwGetTime();

    if(currentTime-lastRecordTime>=1){
        double FPS = (double)framesSinceLastRecord/(currentTime-lastRecordTime);
        std::string windowText = name + "  " + std::to_string(FPS) + " FPS";

        glfwSetWindowTitle(window, windowText.c_str());
        lastRecordTime = currentTime;
        framesSinceLastRecord = 0;
    }

    printGLError();
    glfwSwapBuffers(window);
    glfwPollEvents();
}


bool GUI::is_pressed(std::string button){
    int button_id = buttonNameToID(button);
    return InputHandler::Handler::instance().keys[button_id] > 0;
}

bool GUI::is_running(){
    return !glfwWindowShouldClose(window);
}

std::tuple<float,float> GUI::get_cursor_pos(){
    float x = InputHandler::Handler::instance().lastX;
    float y = InputHandler::Handler::instance().lastY;
    //printf("in cpp :    %f  %f\n",x,y);
    x = x / (float) W;
    y = (H-y) / (float) H;
    return std::make_tuple(x,y);
}

bool GUI::get_event(int tag){
    glfwPollEvents();
    if(events.size()==0){
        return false;
    }
    if(tag == EVENT_NONE){
        currentEvent = events.front();
        events.pop_front();
        return true;
    }
    else{
        std::list<Event>::iterator it;
        for (it = events.begin(); it != events.end(); ++it){
            if(it -> tag == tag){
                currentEvent = *it;
                events.erase(it);
                return true;
            }
        }
        return false;
    }
}

// these 2 are used to export the `currentEvent` field to python
Event GUI::getCurrentEvent(){
    return currentEvent;
}
void GUI::setCurrentEvent(const Event& event){
    currentEvent = event;
}

void GUI::initCudaTexture(){
    struct cudaGraphicsResource *resource;
    cudaArray *array;

    HANDLE_ERROR(cudaGraphicsGLRegisterImage(&resource, textureID, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsSurfaceLoadStore));

    glBindTexture( GL_TEXTURE_2D, 0);

    HANDLE_ERROR(cudaGraphicsMapResources(1, &resource, 0));

    HANDLE_ERROR(cudaGraphicsSubResourceGetMappedArray(&array, resource, 0,0));

    struct cudaResourceDesc description;
    memset(&description, 0, sizeof(description));
    description.resType = cudaResourceTypeArray;
    description.res.array.array = array;

    HANDLE_ERROR(cudaCreateSurfaceObject((cudaSurfaceObject_t *) (&surface), &description));
}

void GUI::initVertexBuffer()
{
        auto vPos_location = glGetAttribLocation(tris->shader->program, "vPos");
        glGenVertexArrays(1,&(tris->VAO));
        glGenBuffers(1, &(tris->VBO));
        glBindVertexArray(tris->VAO);
        glBindBuffer(GL_ARRAY_BUFFER, tris->VBO);
        numTris = 8;
        // initialize buffer object
        unsigned int bufferSize = numTris * 6 * sizeof(float);
        glBufferData(GL_ARRAY_BUFFER, bufferSize, NULL,  GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(vPos_location);
        //stride is 0, so we should store vertex as : [x,y],[x,y]
        glVertexAttribPointer(vPos_location, 2, GL_FLOAT, GL_FALSE, 0 , (void* )(sizeof(float) * 0)); 
        glBindVertexArray(0);

        // register this buffer object with CUDA
        HANDLE_ERROR(cudaGraphicsGLRegisterBuffer(&cuda_vbo_resource, tris->VBO, cudaGraphicsMapFlagsWriteDiscard));
}
void GUI::updateVertexBuffer()
{
    // TODO: need to evalute the correctness
    
    auto vPos_location = glGetAttribLocation(tris->shader->program, "vPos");
    glBindVertexArray(tris->VAO);
    // If we want to resize the vertex buffer, we don't need to delete VBO
    // Just call glBindBuffer: https://stackoverflow.com/questions/18210396/opengl-how-to-unbind-and-delete-buffers-properly
    glBindBuffer(GL_ARRAY_BUFFER, tris->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6 * numTris, NULL,  GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(vPos_location);
    glVertexAttribPointer(vPos_location, 2, GL_FLOAT, GL_FALSE, 0 , (void* )(sizeof(float) * 0)); //stride is 0, so we should store vertex as : [x,y],[x,y]
    glBindVertexArray(0);
}

GUI::~GUI(){
    delete quad;
    delete tris;
    glfwDestroyWindow(window);
}

