#pragma once
#include <memory>
#include <functional>
#include <vector>
#include "Utils.h"

namespace InputHandler{

    //Singleton
	class Handler {
	public:
		static Handler& instance()
		{
			static Handler   instance; 
			return instance;
		}

		bool keys[1024];
		float lastX = 0;
		float lastY = 0;
		bool firstMouse = true;

		bool leftMouseDown = false;


		std::vector<std::function<void(int,int)>> userKeyCallbacks;
		std::vector<std::function<void(double,double)>> userMousePosCallbacks;
		std::vector<std::function<void(int,int)>> userMouseButtonCallbacks;

		
		void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
		{
			if (action == GLFW_PRESS) {
				keys[key] = true;

			}
			else if (action == GLFW_RELEASE) {
				keys[key] = false;
			}
			for(auto f:userKeyCallbacks){
				f(key,action);
			}
		}

		void mousePosCallback(GLFWwindow* window, double xpos, double ypos)
		{
			if (firstMouse)
			{
				lastX = xpos;
				lastY = ypos;
				firstMouse = false;
			}

			GLfloat dx = xpos - lastX;
			GLfloat dy = lastY - ypos;

			lastX = xpos;
			lastY = ypos;

			for(auto f:userMousePosCallbacks){
				f(xpos,ypos);
			}

		}

		void mouseButtonCallback(GLFWwindow* window, int button, int action, int modifier)
		{
			if (button == GLFW_MOUSE_BUTTON_LEFT) {
				if (action == GLFW_PRESS) {
					leftMouseDown = true;
					glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
				}
				if (action == GLFW_RELEASE) {
					leftMouseDown = false;
					glfwSetInputMode(window, GLFW_CURSOR,GLFW_CURSOR_NORMAL);
				}
			}
            if (action == GLFW_PRESS) {
				keys[button] = true;
			}
			else if (action == GLFW_RELEASE) {
				keys[button] = false;
			}
			for(auto f:userMouseButtonCallbacks){
				f(button,action);
			}
		}

	public:
		Handler(Handler const&) = delete;
		void operator=(Handler const&) = delete;

	private:
		Handler() {


		}
	};
    

    inline void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode)
    {
		Handler::instance().keyCallback(window, key, scancode, action, mode);
    }

    inline void mousePosCallback(GLFWwindow* window, double xpos, double ypos)
    {
		Handler::instance().mousePosCallback(window, xpos,ypos);
    }

	inline void mouseButtonCallback(GLFWwindow* window, int button, int action, int modifier)
	{
		Handler::instance().mouseButtonCallback(window, button,action,modifier);
	}

};
