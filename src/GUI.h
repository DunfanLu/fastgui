#pragma once

#include <string>
#include "Utils.h"
#include "InputHandler.h"

#include <vector>
#include <unordered_map>
#include <queue>
#include <list>
#include <tuple>


#define DTYPE_F32 0
#define DTYPE_U8 1
#define DTYPE_I32 3
#define EVENT_NONE 0
#define EVENT_PRESS 1


struct Event{
    int tag;
    std::string key;

    std::string getKey(){
        return key;
    }
    void setKey(const std::string& key_){
        key = key_;
    }
};

struct GUI{
    int W;
    int H;
    std::string name;
    GLFWwindow* window;

    struct Quad* quad;
    struct Triangle* tris;
    GLuint textureID;
    GLuint triColor;
    GLuint numTris;
    // used for FPS counting
    double lastRecordTime;
    int framesSinceLastRecord = 0;

    std::list<Event> events;
    Event currentEvent = {EVENT_NONE,""};

    
    struct cudaSurfaceObject* surface;
    
    struct cudaGraphicsResource *cuda_vbo_resource;

    GUI(std::string name_, int W_, int H_);

    void setCallbacks();

    void initTexture();

    void drawBuffer();

    void set_image(uint64_t image,int channels,int dtype);

    void triangles(uint64_t vert1Addr, uint64_t vert2Addr, uint64_t vert3Addr, int dtype, int numTriangles, uint32_t color);
    
    void show();


    bool is_pressed(std::string button);

    bool is_running();

    std::tuple<float,float> get_cursor_pos();

    bool get_event(int tag);

    // these 2 are used to export the `currentEvent` field to python
    Event getCurrentEvent();
    void setCurrentEvent(const Event& event);

    // Init a vertex buffer to draw triangles
    void initVertexBuffer();
    // If the vetext buffer is not large enough, we delete the 
    // original vertex buffer and recreate a new vertex buffer
    void updateVertexBuffer();
    void initCudaTexture();

    ~GUI();
};


