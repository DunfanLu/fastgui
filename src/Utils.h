

#pragma once

#include <string>
#include <iostream>
#include <stdexcept>
#include <stdio.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>


#include <stdarg.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtc/type_ptr.hpp>





inline void printGLError(){
    GLenum err=glGetError();
    std::string error;
    switch(err) {
        case GL_INVALID_OPERATION:      error="INVALID_OPERATION";      break;
        case GL_INVALID_ENUM:           error="INVALID_ENUM";           break;
        case GL_INVALID_VALUE:          error="INVALID_VALUE";          break;
        case GL_OUT_OF_MEMORY:          error="OUT_OF_MEMORY";          break;

        case GL_INVALID_FRAMEBUFFER_OPERATION: error="GL_INVALID_FRAMEBUFFER_OPERATION"; break;
    }

    if(err!=GL_NO_ERROR){
        std::cerr <<"GL Error :  "<<error<<std::endl;
    }
}


inline void checkFramebufferComplete()
{
	GLenum err = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	const char* errString = NULL;
	switch (err) {
        case GL_FRAMEBUFFER_UNDEFINED: errString = "GL_FRAMEBUFFER_UNDEFINED"; break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: errString = "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"; break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: errString = "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"; break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER: errString = "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"; break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER: errString = "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER"; break;
        case GL_FRAMEBUFFER_UNSUPPORTED: errString = "GL_FRAMEBUFFER_UNSUPPORTED"; break;
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE: errString = "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"; break;
        case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS: errString = "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS"; break;
	}

	if (errString) {
		fprintf(stderr, "OpenGL Framebuffer Error #%d: %s\n", err, errString);
	}
}


inline void initOpenGL() {
	if (!glfwInit()){
        printf("cannot initialize GLFW\n");
        exit(EXIT_FAILURE);
    }
    
}


inline void getScreenDimensions(int& width, int& height) {
	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);

	width = mode->width;
	height = mode->height;
}


inline GLFWwindow* createWindowOpenGL(const std::string& name,int screenWidth,int screenHeight){
    initOpenGL();
    GLFWwindow* window;
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    window = glfwCreateWindow(screenWidth, screenHeight, name.c_str(), nullptr, nullptr);

    if (!window )
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("cannot initialize GLAD\n");
        exit(EXIT_FAILURE);
    }
    
    printGLError();
    glfwSwapInterval(0);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    // float ratio;
    // ratio = width / (float) height;

    // Setup some OpenGL options
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glViewport(0, 0, width, height);
    return window;
}

inline int buttonNameToID(const std::string& name){
    if(name == "LMB"){
        return GLFW_MOUSE_BUTTON_LEFT;
    }
    if(name == "RMB"){
        return GLFW_MOUSE_BUTTON_RIGHT;
    }
    if(name.size()==1){
        char c = name[0];
        if (c >= 'a' && c <= 'z'){
            c = c - ('a' - 'A');
        }
        return (int)c;
    }
    if(name == "Control"){
        return GLFW_KEY_LEFT_CONTROL;
    }
    if(name == "Shift"){
        return GLFW_KEY_LEFT_SHIFT;
    }
    if (name == " "){
        return GLFW_KEY_SPACE;
    }
    if (name == "Return"){
        return GLFW_KEY_ENTER;
    }
    printf("unrecognized button name: %s\n",name.c_str());
    exit(1);
}

inline std::string buttonIDToName(int id){
    if (id >= 'A' && id <= 'Z'){
        char c = id + ('a' - 'A');
        std::string name;
        name += c;
        return name;
    }
    else if (id==GLFW_MOUSE_BUTTON_LEFT){
        return "LMB";
    }
    else if (id==GLFW_MOUSE_BUTTON_RIGHT){
        return "RMB";
    }
    else if (id == GLFW_KEY_LEFT_CONTROL || id == GLFW_KEY_RIGHT_CONTROL){
        return "Control";
    }
    else if (id == GLFW_KEY_LEFT_SHIFT || id == GLFW_KEY_RIGHT_SHIFT){
        return "Shift";
    }
    else if (id == GLFW_KEY_SPACE){
        return " ";
    }
    else if (id == GLFW_KEY_ENTER){
        return "Return";
    }
    printf("unrecognized button id: %d\n",id);
    exit(1);
}

inline int nextPowerOf2(int n)
{
    int count = 0;
     
    if (n && !(n & (n - 1)))
        return n;
     
    while( n != 0)
    {
        n >>= 1;
        count += 1;
    }
     
    return 1 << count;
}


inline int divUp(int a, int b) {
	if (b == 0) {
		return 1;
	}
	int result = (a % b != 0) ? (a / b + 1) : (a / b);
	return result;
}

