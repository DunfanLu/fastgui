
#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Utils.h"
#include <vector>

class Shader
{
public:
	static std::string SHADERS_PATH(const std::string& file = "") { 
		return "./resources/Shaders/" + file; 
	}

	static std::string readTextFile(const std::string& path) {
		std::string result;
		std::ifstream file;
		file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		file.open(path);

		std::stringstream stream;

		stream << file.rdbuf();

		result = stream.str();
		return result;
	}

	static std::string readTextFiles(const std::vector<std::string>& paths) {
		std::string result = "";
		for (const std::string& path : paths) {
			result += readTextFile(path) + "\n";
		}

		return result;
	}

public:
    GLuint program;
	Shader(const std::string& vertexSource, const std::string& fragmentSource);

	void use() { glUseProgram(this->program); }

	~Shader();

	GLint getUniformLocation(std::string name);

	void setUniform1i(std::string name,int val,bool debug = false);
	void setUniform1f(std::string name,float val, bool debug = false);

	void setUniformMat4(std::string name, const glm::mat4& mat, bool debug = false);
	void setUniform3f(std::string name, float3 val, bool debug = false);
	void setUniform3f(std::string name, glm::vec3 val, bool debug = false);
	void setUniform4f(std::string name, float4 val, bool debug = false);
	void setUniform4f(std::string name, glm::vec4 val, bool debug = false);
	

private:
	void checkCompileErrors(GLuint shader, std::string type);
};
 