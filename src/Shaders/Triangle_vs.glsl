static std::string Triangle_vs = R"###(

#version 330 core
layout (location = 0) in vec2 vPos; 
void main()
{
    vec4 pos = vec4(vPos.x*2-1,vPos.y*2-1,0.0,1.0);
    gl_Position = pos; 
}
)###"; 