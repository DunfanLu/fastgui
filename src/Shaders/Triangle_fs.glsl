static std::string Triangle_fs = R"###(

#version 330 core
uniform vec3 vertexColor;

out vec4 out_color;
void main()
{
    out_color = vec4(vertexColor, 1.0);
} 

)###"; 