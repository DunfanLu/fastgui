#pragma once
#include <string>

#include "Shader.h"
#include <memory>

#include "Shaders/Triangle_vs.glsl"
#include "Shaders/Triangle_fs.glsl"

struct Triangle {

    std::shared_ptr<Shader> shader;

    GLuint VBO,VAO;
    GLint vPos_location, texCoord_location;

    Triangle (){
        shader = std::make_shared<Shader>(Triangle_vs,Triangle_fs);
    }
    void draw(GLuint color, uint32_t numTriangles){
        // Converte HEX color to RGB color
        // https://stackoverflow.com/questions/3723846/convert-from-hex-color-to-rgb-struct-in-c
        glm::vec3 rgbColor;
        rgbColor.r = ((color >> 16) & 0xFF) / 255.0;  // Extract the RR byte
        rgbColor.g = ((color >> 8) & 0xFF) / 255.0;   // Extract the GG byte
        rgbColor.b = ((color) & 0xFF) / 255.0;        // Extract the BB byte
        glUseProgram(shader->program);
        glBindVertexArray(VAO);
		shader->setUniform3f("vertexColor", rgbColor);
        
        glDrawArrays(GL_TRIANGLES,0, 3 * numTriangles);

        glBindVertexArray(0);
    }

	~Triangle() {
		glDeleteBuffers(1, &VBO);
		glDeleteVertexArrays(1, &VAO);
	}
	
};
