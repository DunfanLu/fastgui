import taichi as ti
import fast_gui
ti.init(arch = ti.cuda)

N = 100
NF = 2 * N**2  # number of faces
NV = (N + 1)**2  # number of vertices
pos = ti.Vector.field(2, float, NV)
f2v = ti.Vector.field(3, int, NF)  # ids of three vertices of each face
a = ti.Vector.field(2, float, NF)
b = ti.Vector.field(2, float, NF)
c = ti.Vector.field(2, float, NF)


@ti.kernel
def init_pos():
    for i, j in ti.ndrange(N + 1, N + 1):
        k = i * (N + 1) + j
        pos[k] = ti.Vector([i, j]) / N  + ti.Vector([-0.5, -0.5])

@ti.kernel
def init_mesh():
    for i, j in ti.ndrange(N, N):
        k = (i * N + j) * 2
        a = i * (N + 1) + j
        b = a + 1
        c = a + N + 2
        d = a + N + 1
        f2v[k + 0] = [a, b, c]
        f2v[k + 1] = [c, d, a]

@ti.kernel
def transfer():
    for i in range(NF):
        ia, ib, ic = f2v[i]
        a[i], b[i], c[i] = pos[ia], pos[ib], pos[ic]

init_pos()
init_mesh()
transfer()
res = (1024,1024)
gui = fast_gui.GUI('Draw Triangles', res)
while gui.running:
    ti.sync()
    if(gui.is_pressed(ti.GUI.LMB)):
        print(gui.get_cursor_pos())
    gui.triangles(a,b,c,color=0xFF0000)
    gui.show()        
