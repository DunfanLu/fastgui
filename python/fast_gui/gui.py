from ._impl import GUI as GUI_Native
from ._impl import DTYPE_F32, DTYPE_U8, DTYPE_I32
from ._impl import EVENT_NONE, EVENT_PRESS

import taichi as ti

@ti.kernel
def get_field_addr2d(x: ti.template()) -> ti.u64:
    return ti.get_addr(x,[0,0])
@ti.kernel
def get_field_addr1d(x: ti.template()) -> ti.u64:
    return ti.get_addr(x,[0])

class GUI(GUI_Native):
    def __init__(self,name,res):
        super().__init__(name,res)
    
    @property
    def running(self):
        return self.is_running()

    def set_image(self,img):
        if hasattr(img,'n'):
            channels = img.n
        else:
            channels = 1
        
        # if channels not in [3,4]:
        #     raise Exception("number of channels can only be 3 or 4")
        if img.dtype == ti.f32:
            dtype = DTYPE_F32
        elif img.dtype == ti.uint8:
            dtype = DTYPE_U8
        else:
            raise Exception("dtype can only be f32 or u8")
        ti.sync()
        addr = get_field_addr2d(img)
        ti.sync()
        super().set_image(addr,channels,dtype)

    def triangles(self,a, b, c, color=0xFFFFFF):
        if hasattr(a, 'shape') and hasattr(b, 'shape') and hasattr(c, 'shape'):
            assert a.shape == b.shape == c.shape, \
                "The shape of the triangle list is not consistent."
        numTriangles = a.shape[0]
        assert a.n == b.n == c.n == 2, \
            "We just support 2 dim triangles rendering."
        if hasattr(a,'dtype') and hasattr(b, 'dtype') and hasattr(c, 'dtype'):
            assert a.dtype == b.dtype == c.dtype, \
                "The type of the triangle vertices is not consistent."        
        
        if a.dtype == ti.f32:
            dtype = DTYPE_F32
        elif a.dtype == ti.i32:
            dtype = DTYPE_I32
        else: 
            raise Exception("dtype for vertices position can only be f32 or i32")
        
        ti.sync()
        vertA = get_field_addr1d(a)
        ti.sync() # TODO: do we need this line?
        vertB = get_field_addr1d(b)
        ti.sync() # TODO: do we need this line?
        vertC = get_field_addr1d(c)
        ti.sync()
        super().triangles(vertA, vertB, vertC, dtype, numTriangles,color)

    def get_event(self,tag = None):
        if tag==None:
            return super().get_event(EVENT_NONE)
        elif tag == ti.GUI.PRESS:
            return super().get_event(EVENT_PRESS)
        raise Exception("unrecognized event tag")
