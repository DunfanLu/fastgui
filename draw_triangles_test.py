import taichi as ti
import fast_gui

res = (512,512)
ti.init(arch = ti.cuda)
nTris = 2
a = ti.Vector.field(2, float, nTris)
b = ti.Vector.field(2, float, nTris)
c = ti.Vector.field(2, float, nTris)


#gui = fast_gui.GUI('Draw Triangles', res)
gui = ti.GUI('Draw Triangles', res)
@ti.kernel
def initTris():
    a[0] = ti.Vector([0.1,0.1])
    b[0] = ti.Vector([0.4,0.1])
    c[0] = ti.Vector([0.1,0.4])
    a[1] = b[0]
    b[1] = ti.Vector([0.4,0.4])
    c[1] = c[0]

initTris()
while gui.running:
    ti.sync()
    if(gui.is_pressed(ti.GUI.LMB)):
        print(gui.get_cursor_pos())
    gui.triangles(a.to_numpy(),b.to_numpy(),c.to_numpy(),color=0xFF0000)
    #gui.triangles(a,b,c,color=0xFF0000)
    gui.show()