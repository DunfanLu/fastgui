import taichi as ti
import fast_gui

res = (512,512)
ti.init(arch = ti.cuda)
img = ti.Vector.field(3, ti.uint8, res)


gui = ti.GUI("heyy",res,fast_gui =  True)

@ti.kernel
def render(frame_id: int):
    for x,y in img:
        img[x,y][0] = x
        img[x,y][1] = y
        img[x,y][2] = 0
        #img[x,y][3] = 255

frame_id = 0
while gui.running:
    frame_id += 1
    frame_id = frame_id % 256
    render(frame_id)
    ti.sync()
    #print(frame_id)
    if(gui.is_pressed(ti.GUI.LMB)):
        print(gui.get_cursor_pos())
    gui.set_image(img)
    gui.show()