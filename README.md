# FastGUI #

A fast gui for taichi programs that avoids redundant GPU-CPU data transfer.

### Installation ###

Clone recursively (the pybind11 git submodule is required), and then run `pip install .` in the root directory of this repo.
